variable "ami" {

  default = {
    "us-east-1" = "ami-09e67e426f25ce0d7"
  }
}

variable "instance_count" {
  default = "2"
}

variable "instance_type" {
  default = "t2.micro"
}

variable "aws_region" {
  default = "us-east-1"
}

variable "server_port" {
  default = "80"
}
