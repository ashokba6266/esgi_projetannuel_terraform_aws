######################################################################################################################################

# S3 # 

######################################################################################################################################




######################################################################################################################################

# VPC # 

######################################################################################################################################

resource "aws_vpc" "my_esgi_infra" {
  cidr_block           = "10.10.0.0/16"
  enable_dns_support   = true
  enable_dns_hostnames = true

  tags = {
    Name = "esgi_vpc"
  }
}

######################################################################################################################################

# INTERNET GATEWAY # 

######################################################################################################################################

resource "aws_internet_gateway" "gw" {
  vpc_id = aws_vpc.my_esgi_infra.id

  tags = {
    Name = "main"
  }
}

######################################################################################################################################

# ROUTE TABLE # 

######################################################################################################################################


resource "aws_route_table" "esgiroute" {
  vpc_id = aws_vpc.my_esgi_infra.id

  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = aws_internet_gateway.gw.id
  }

  tags = {
    Name = "Route table for esgi project"
  }
}

resource "aws_route_table" "esgi_private_route_table" {
  vpc_id = aws_vpc.my_esgi_infra.id
}

######################################################################################################################################

# ROUTE TABLE ASSOCIATION # 

######################################################################################################################################

resource "aws_route_table_association" "public_one" {
  subnet_id      = aws_subnet.my_subnet_one_public.id
  route_table_id = aws_route_table.esgiroute.id
}

resource "aws_route_table_association" "public_two" {
  subnet_id      = aws_subnet.my_subnet_two_public.id
  route_table_id = aws_route_table.esgiroute.id
}


resource "aws_route_table_association" "private_one" {
  subnet_id      = aws_subnet.my_subnet_one_private.id
  route_table_id = aws_route_table.esgi_private_route_table.id
}

resource "aws_route_table_association" "private_two" {
  subnet_id      = aws_subnet.my_subnet_two_private.id
  route_table_id = aws_route_table.esgi_private_route_table.id
}


######################################################################################################################################

#APPLICATION LOAD BALANCER#

# Create a target group  

######################################################################################################################################


resource "aws_lb_target_group" "my-tg" {

  /*
  health_check {
    interval            = 10
    path                = "/"
    port                = 80
    protocol            = "HTTP"
    timeout             = 5
    healthy_threshold   = 5
    unhealthy_threshold = 2
  }
*/
  name        = "tf-lb"
  port        = "80"
  protocol    = "TCP"
  target_type = "instance"
  vpc_id      = aws_vpc.my_esgi_infra.id
}

# PLACE LOAD BALANCER IN FRONT OF EC2
resource "aws_lb_target_group_attachment" "group1" {
  target_group_arn = aws_lb_target_group.my-tg.id
  target_id        = aws_instance.my_ec2_one.id
  port             = "80"
}

# PLACE LOAD BALANCER IN FRONT OF EC2
resource "aws_lb_target_group_attachment" "group2" {
  target_group_arn = aws_lb_target_group.my-tg.id
  target_id        = aws_instance.my_ec2_two.id
  port             = "80"
}

# LOAD BALANCER
resource "aws_lb" "my-alb" {
  name               = "my-alb"
  internal           = false
  load_balancer_type = "network"

  subnets = [aws_subnet.my_subnet_one_public.id, aws_subnet.my_subnet_two_public.id]

  enable_deletion_protection = false
  ip_address_type            = "ipv4"

  tags = {
    Environment = "production"
  }
}


# THIS DESCRIBES IN WHICH PORT THIS LOAD BALANCER IS GOING TO LISTEN 
resource "aws_lb_listener" "front_end" {
  load_balancer_arn = aws_lb.my-alb.id
  port              = "8000"
  protocol          = "TCP"

  default_action {
    type             = "forward"
    target_group_arn = aws_lb_target_group.my-tg.id
  }
}

####################################################################################################################################

#SUBNETS#

####################################################################################################################################

resource "aws_subnet" "my_subnet_one_public" {
  vpc_id            = aws_vpc.my_esgi_infra.id
  cidr_block        = "10.10.1.0/24"
  availability_zone = "us-east-1a"

  tags = {
    Name = "esgi_subnet_one"
  }
}

resource "aws_subnet" "my_subnet_two_public" {
  vpc_id            = aws_vpc.my_esgi_infra.id
  cidr_block        = "10.10.2.0/24"
  availability_zone = "us-east-1b"

  tags = {
    Name = "esgi_subnet_two"
  }
}

resource "aws_subnet" "my_subnet_one_private" {
  vpc_id            = aws_vpc.my_esgi_infra.id
  cidr_block        = "10.10.3.0/24"
  availability_zone = "us-east-1a"

  tags = {
    Name = "esgi_subnet_two"
  }
}

resource "aws_subnet" "my_subnet_two_private" {
  vpc_id            = aws_vpc.my_esgi_infra.id
  cidr_block        = "10.10.4.0/24"
  availability_zone = "us-east-1b"

  tags = {
    Name = "esgi_subnet_two"
  }
}

####################################################################################################################################

#EC2 INSTANCES#

####################################################################################################################################

resource "aws_instance" "my_ec2_one" {

  ami                    = "ami-0ab4d1e9cf9a1215a"
  instance_type          = "t2.micro"
  key_name               = "esgi-machines"
  subnet_id              = aws_subnet.my_subnet_one_public.id
  vpc_security_group_ids = [aws_security_group.security_group.id]


  associate_public_ip_address = true


  tags = {
    Name = "sogeti_ec2_instance_one"
  }
}

resource "aws_instance" "my_ec2_two" {
  ami                    = "ami-0ab4d1e9cf9a1215a"
  instance_type          = "t2.micro"
  key_name               = "esgi-machines"
  subnet_id              = aws_subnet.my_subnet_two_public.id
  vpc_security_group_ids = [aws_security_group.security_group.id]
  #security_groups             = [aws_security_group.security_group.id]
  associate_public_ip_address = true

  tags = {
    Name = "sogeti_ec2_instance_two"
  }
}

####################################################################################################################################

#SECURITY GROUP#

####################################################################################################################################

resource "aws_security_group" "security_group" {
  name        = "allow_tls"
  description = "Allow TLS inbound traffic"
  vpc_id      = aws_vpc.my_esgi_infra.id

  ingress {
    description = "TLS from VPC"
    from_port   = 443
    to_port     = 443
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
    description = "TLS from VPC"
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
    from_port   = 80
    to_port     = 80
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }
  ingress {
    from_port   = 8000
    to_port     = 8000
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    description = ""
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags = {
    Name = "allow_tls"
  }

}


####################################################################################################################################

#SECURITY GROUP FOR RDS INSTANCES#

####################################################################################################################################

resource "aws_security_group" "security_group_db" {
  name        = "allow_tls_db"
  description = "Allow TLS inbound traffic for db instances"
  vpc_id      = aws_vpc.my_esgi_infra.id

  ingress {
    from_port   = 3306
    to_port     = 3306
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  # Allow all outbound traffic.
  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags = {
    Name = "allow_tls"
  }


}

####################################################################################################################################

#RDS INSTANCES#

####################################################################################################################################

resource "aws_db_instance" "db_mysql_one" {
  allocated_storage      = 10
  engine                 = "mysql"
  engine_version         = "5.7"
  instance_class         = "db.t2.micro"
  name                   = "mydb"
  username               = "foo"
  password               = "foobarbaz"
  parameter_group_name   = "default.mysql5.7"
  skip_final_snapshot    = true
  db_subnet_group_name   = aws_db_subnet_group.esgi_db_subnet_group.id
  vpc_security_group_ids = [aws_security_group.security_group_db.id]
}

/*
resource "aws_db_instance" "db_mysql_two" {
  allocated_storage      = 10
  engine                 = "mysql"
  engine_version         = "5.7"
  instance_class         = "db.t2.micro"
  name                   = "mydb"
  username               = "foo"
  password               = "foobarbaz"
  parameter_group_name   = "default.mysql5.7"
  skip_final_snapshot    = true
  db_subnet_group_name   = aws_db_subnet_group.esgi_db_subnet_group.id
  vpc_security_group_ids = [aws_security_group.security_group_db.id]
}
*/

resource "aws_db_subnet_group" "esgi_db_subnet_group" {
  name       = "db subnet group"
  subnet_ids = [aws_subnet.my_subnet_one_public.id, aws_subnet.my_subnet_one_public.id, aws_subnet.my_subnet_one_private.id, aws_subnet.my_subnet_two_private.id]

  #subnet_ids = [aws_subnet.my_subnet_one_public.id, aws_subnet.my_subnet_two_public.id]

  tags = {
    Name = "My DB subnet group"
  }
}

####################################################################################################################################

#CLOUD WATCH#

####################################################################################################################################

resource "aws_cloudwatch_metric_alarm" "ec2_alarms" {
  alarm_name                = "terraform-test-foobar5"
  comparison_operator       = "GreaterThanOrEqualToThreshold"
  evaluation_periods        = "2"
  metric_name               = "CPUUtilization"
  namespace                 = "AWS/EC2"
  period                    = "120"
  statistic                 = "Average"
  threshold                 = "80"
  alarm_description         = "This metric monitors ec2 cpu utilization"
  insufficient_data_actions = []
}

####################################################################################################################################

# OUTPUT #

####################################################################################################################################


/*
output "public_ip_master" {
  description = "List of public IP addresses assigned to the instances, if applicable"
  value       = aws_instance.AppServer.*.public_ip
}

output "public_ip_serverA" {
  description = "List of public IP addresses assigned to the instances, if applicable"
  value       = aws_instance.serverA.*.public_ip
}

output "public_ip_serverB" {
  description = "List of public IP addresses assigned to the instances, if applicable"
  value       = aws_instance.serverB.*.public_ip
}
*/
